# The FreeDOS Kernel

# Still Actively Developed

Unsure which is official kernel source tree

[https://github.com/FDOS/kernel](https://github.com/FDOS/kernel) or [https://github.com/PerditionC/fdkernel](https://github.com/PerditionC/fdkernel)

# Original home

[https://svn.code.sf.net/p/freedos/svn/kernel/](https://svn.code.sf.net/p/freedos/svn/kernel/)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## KERNEL.LSM

<table>
<tr><td>title</td><td>The FreeDOS Kernel</td></tr>
<tr><td>version</td><td>2043</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-05-14</td></tr>
<tr><td>description</td><td>The FreeDOS Kernel</td></tr>
<tr><td>summary</td><td>The FreeDOS Kernel (supports FAT12/FAT16/FAT32). Includes COUNTRY.SYS, SETVER.SYS and SYS.COM.</td></tr>
<tr><td>keywords</td><td>kernel, FreeDOS, DOS, MSDOS</td></tr>
<tr><td>author</td><td>(developers: can be reached on the freedos-kernel mailing list)</td></tr>
<tr><td>maintained&nbsp;by</td><td>freedos-devel@lists.sourceforge.net</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/kernel</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.fdos.org/kernel/</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://svn.code.sf.net/p/freedos/svn/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS, DOSEMU (OpenWatcom C or Turbo C, NASM, UPX)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
